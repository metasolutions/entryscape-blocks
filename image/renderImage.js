define([
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-style",
    "entryscape-blocks/utils/getEntry"
], function(domAttr, domConstruct, domStyle, getEntry, getTextContent) {

    return function(node, data, items) {
        getEntry(data, function(entry) {
            domAttr.set(node, "innerHTML", "");
            var src;
            if (data.property) {
                src = entry.getMetadata().findFirstValue(entry.getResourceURI(), data.property);
            } else {
                src = entry.getResourceURI();
            }
            var _node = domConstruct.create("img", {src: src}, node);
            if (data.width) {
                domStyle.set(_node, "width", data.width);
            }
            if (data.height) {
                domStyle.set(_node, "height", data.height);
            }
        });
    };
});