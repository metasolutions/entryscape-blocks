define([
    'dojo/dom-attr',
    'dojo/dom-class',
    'dojo/on',
], (domAttr, domClass, on) => {
    return function(node, data) {
        domAttr.set(node, 'innerHTML', 'Blocks error, click to see details.');
        domAttr.set(node, 'title', data.error);
        domClass.add(node, 'entryscape-boot-error');
        on(node, "click", () => {
            alert(data.error+ "\n"+data.errorCause);
        })
    };
});