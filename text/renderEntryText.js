define([
    "dojo/dom-attr",
    "entryscape-blocks/utils/getEntry",
    "entryscape-blocks/utils/getTextContent"
], function(domAttr, getEntry, getTextContent) {

    return function(node, data, items) {
        getEntry(data, function(entry) {
            domAttr.set(node, "innerHTML", getTextContent(data, entry) || data.fallback || "");
        });
    };
});