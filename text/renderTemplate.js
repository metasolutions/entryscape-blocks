define([
    "dojo/dom-construct",
    "dojo/dom-attr",
    "entryscape-blocks/boot/params",
    "entryscape-blocks/boot/handlebars",
    "entryscape-blocks/utils/getEntry",
], function(domConstruct, domAttr, params, handlebars, getEntry) {

    return function(node, data, items) {
        getEntry(data, function(entry) {
            handlebars.run(node, data, null, entry);
        });
    };
});