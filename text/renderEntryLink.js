define([
    "dojo/dom-construct",
    "dojo/dom-attr",
    "entryscape-blocks/boot/params",
    "entryscape-blocks/utils/getEntry",
    "entryscape-blocks/utils/getTextContent"
], function(domConstruct, domAttr, params, getEntry, getTextContent) {

    return function(node, data, items) {
        getEntry(data, function(entry) {
            let pobj = {};
            const md = entry.getMetadata();
            if (data.clickkey && data.clickvalue) {
                if (data.clickentry) {
                    pobj.entry = entry.getId();
                    pobj.context = entry.getContext().getId();
                }
                let mdValue;
                switch (data.clickvalue) {
                    case 'resource':
                        pobj[data.clickkey] = entry.getResourceURI();
                        break;
                    default:
                        mdValue = md.findFirstValue(entry.getResourceURI(), data.clickvalue);
                        if (mdValue && mdValue !== "") {
                            pobj[data.clickkey] = mdValue;
                        }
                }
            } else {
                pobj = {entry: entry.getId(), context: entry.getContext().getId()};
            }
            domAttr.set(node, "innerHTML", "");
            _node = domConstruct.create("a", {
               href: params.getLink(data.click, pobj),
               innerHTML: getTextContent(data, entry)
           }, node);
        });
    };
});