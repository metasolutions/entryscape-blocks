define([
  'dojo/dom-construct',
  'dojo/dom-attr',
  'dojo/dom-style',
  'store/EntryStoreUtil',
  './Graph',
  './triples',
  '../utils/getEntry',
], (domConstruct, domAttr, domStyle, EntryStoreUtil, Graph, triples, getEntry) =>
  function (node, data, items) {
    getEntry(data, (entry) => {
      const excludeArr = ['rdf:type', 'skos:broader', 'skos:inScheme', 'skos:topConceptOf'];
      const g = new Graph(node, triples.createLoader(excludeArr));
      g.load(entry.getResourceURI());
    });
  });
