define([
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-style",
    "entryscape-blocks/boot/params",
    "entryscape-blocks/boot/handlebars",
    "entryscape-blocks/utils/getEntry",
    "leaflet"
], function(domConstruct, domAttr, domStyle, params, handlebars, getEntry, leaflet) {

    leaflet.Icon.Default.imagePath = 'https://static.entryscape.com/libs/leaflet/dist/images/';
    return function(node, data, items) {
        getEntry(data, function(entry) {
            setTimeout(function() {
                domStyle.set(node, "height", data.height ? data.height : "300px");
                domStyle.set(node, "display", "block");
                const map = leaflet.map(node).setView([0, 0], 1);
                leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                    maxZoom: 18,
                }).addTo(map);
                const md = entry.getMetadata();
                const lat = md.findFirstValue(entry.getResourceURI(), 'http://www.w3.org/2003/01/geo/wgs84_pos/lat');
                const long = md.findFirstValue(entry.getResourceURI(), 'http://www.w3.org/2003/01/geo/wgs84_pos/long');

                const latlong = [parseFloat(lat.replace(",", ".")), parseFloat(long.replace(",", "."))];
                leaflet.marker(latlong).addTo(map);
                map.setView(latlong, data.zoom ? parseInt(data.zoom) : 8);
            }, data.delay ? parseInt(data.delay) : 1);
        });
    };
});