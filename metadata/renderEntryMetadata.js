define([
    "dojo/dom-construct",
    "dojo/dom-attr",
    "entryscape-blocks/utils/getEntry",
    "rdforms/view/Presenter",
    "rdfjson/namespaces",
], function(domConstruct, domAttr, getEntry, Presenter, namespaces) {

    return function(node, data, items) {
        var template = items.getItem(data.template);
        getEntry(data, function(entry) {
            var fp = {};
            if (data.filterpredicates) {
                data.filterpredicates.split(",").forEach(function(p) {
                    fp[namespaces.expand(p)] = true;
                });
            }
            domAttr.set(node, "innerHTML", "");
            var presenter = new Presenter({compact: data.onecol !== true,
                filterPredicates: fp}, domConstruct.create("div", null, node));
            presenter.show({
                resource: entry.getResourceURI(),
                graph: entry.getMetadata(),
                template: template
            });
        });
    };
});