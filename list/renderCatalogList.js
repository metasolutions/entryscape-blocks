define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-attr",
    "./List",
    "entryscape-commons/list/EntryRow",
    "./MetadataExpandRow",
    "entryscape-commons/defaults",
    "entryscape-commons/store/ArrayList"
], function(declare, lang, domConstruct, domStyle, domAttr, List, EntryRow, MetadataExpandRow, defaults, ArrayList) {

    var _CatalogRowMixin = declare([], {
        showCol1: true,
        showCol3: false,
        renderCol1: function() {
            domStyle.set(this.col1Node, "text-align", "left");
            domAttr.set(this.col1Node, "innerHTML", '<span class="badge">' + this.entry.__nrOfDatasets + '</span>');
        },
        updateLocaleStrings: function() {
            this.inherited(arguments);
            this.renderCol1();
        }
    });

    var CatalogList = declare([List], {
        updateRowClass: function() {
            if (this.conf.template != null) {
                this.rowClass = declare([MetadataExpandRow, _CatalogRowMixin], {});
                return true;
            } else {
                this.rowClass = declare([EntryRow, _CatalogRowMixin], {});
            }
        },

        search: function() {
            var catalogs = [];
            defaults.get("entrystore").newSolrQuery()
                .rdfType("dcat:Catalog").uriProperty("dcat:dataset", "[* TO *]")
                .list().forEach(function(catalogEntry) {
                catalogEntry.__nrOfDatasets = catalogEntry.getMetadata().find(catalogEntry.getResourceURI(), "dcat:dataset").length;
                catalogs.push(catalogEntry);
            }).then(lang.hitch(this, function() {
                catalogs.sort(function(c1, c2) {
                    return c1.__nrOfDatasets < c2.__nrOfDatasets ? 1 : -1;
                });
                this.listView.showEntryList(new ArrayList({arr: catalogs}));
            }));
        }
    });

    return function(node, data, items) {
        var cl = new CatalogList({conf: data, itemstore: items}, domConstruct.create("div", null, node));
        cl.show();
    };
});