define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-style',
  'config',
  'entryscape-commons/list/List',
  'entryscape-commons/list/ListView',
  'entryscape-commons/list/EntryRow',
  'entryscape-commons/defaults',
  'entryscape-commons/store/ArrayList',
  'entryscape-blocks/boot/handlebars',
  'entryscape-blocks/boot/error',
  'entryscape-blocks/utils/constraints',
  'entryscape-blocks/utils/dependencyList',
  'entryscape-blocks/utils/filter',
  './MetadataExpandRow',
  './TemplateExpandRow',
  'di18n/NLSMixin',
], (declare, domConstruct, domClass, domStyle, config, List, ListView, EntryRow, defaults, ArrayList,
             handlebars, error, constraints, dependencyList, filter, MetadataExpandRow, TemplateExpandRow, NLSMixin) => {
  const PlaceHolder = declare([], {
    constructor(args, node) {
      this.list = args.list;
      this.domNode = node;
      const conf = this.list.conf;
      if (!conf.templates || !conf.templates.listplaceholder) {
        return;
      }
      handlebars.run(this.domNode, {
        htemplate: conf.templates.listplaceholder,
        context: conf.context,
        entry: conf.entry,
      });
      if (!this.list.includeHead) {
        domConstruct.place(this.domNode, this.list.domNode);
      }
    },
    show() {
      if (!this.list.includeHead) {
        domStyle.set(this.list.getView().domNode, 'display', 'none');
        if (this.list.listHeadNode) {
          domStyle.set(this.list.listHeadNode, 'display', 'none');
        }
      }
      domStyle.set(this.domNode, 'display', 'block');
    },
    hide() {
      if (!this.list.includeHead) {
        domStyle.set(this.list.getView().domNode, 'display', 'block');
        if (this.list.listHeadNode) {
          domStyle.set(this.list.listHeadNode, 'display', 'block');
        }
      }
      domStyle.set(this.domNode, 'display', 'none');
    },
  });

  const initExpandTitles = function () {
    this.expandTitle = this.list.conf.expandTooltip;
    this.unexpandTitle = this.list.conf.unexpandTooltip;
  };

  const ListRow = declare([EntryRow], {
    showCol3: false,
    postCreate() {
      domClass.add(this.rowNode, 'col4hidden');
      this.inherited(arguments);
    },
    initExpandTitles,
    render() {
      const conf = this.list.conf;
      if (!conf.templates || !conf.templates.rowhead) {
        return this.inherited(arguments);
      }
      handlebars.run(this.nameNode, {
        htemplate: conf.templates.rowhead,
        context: this.entry.getContext().getId(),
        entry: this.entry.getId(),
      }, null, this.entry);
    },
    getRenderNameHTML() {
      const name = this.getRenderName();
      const href = this.list.getRowClickLink(this);
      return href ? `<a href="${href}">${name}</a>` : name;
    },
  });

  return declare([List, NLSMixin.Dijit], {
    includeHead: false,
    includeCreateButton: false,
    includeInfoButton: false,
    includeEditButton: false,
    includeRemoveButton: false,
    searchInList: true,
    nlsBundles: ['escoList', 'escoErrors'],
    rowClickDefault: true,
    placeholderClass: PlaceHolder,
    contextId: null,
    rowClass: ListRow,

//        rowClassPlain: EntryRow,
//        rowClassExpand: MetadataExpandRow,
    conf: null,

    postCreate() {
      const data = this.conf;
      this.listViewClass = declare([ListView], {
        showEntryList(list) {
          dependencyList(list, data);
          this.inherited(arguments);
        },
      });

      this.rowClickDefault = this.conf.click != null;
      if (this.conf.htemplate && !this.conf.templates) {
        try {
          this.conf.templates = handlebars.unGroup(this.conf.htemplate);
        } catch (e) {
          this.conf.error = e.toString();
          this.conf.errorCode = 3;
          this.conf.errorCause = this.conf.htemplate;
          error(this.domNode, this.conf);
          return;
        }
      }
      if (this.conf.templates && this.conf.templates.listhead) {
        this.listHeadNode = domConstruct.create('div', null, this.domNode);
        handlebars.run(this.listHeadNode, this.conf, this.conf.templates.listhead, this.entry);
      }
      if (this.updateRowClass()) {
        this.registerRowAction({
          name: 'expand',
          button: 'link',
          iconType: 'fa',
          icon: 'chevron-down',
        });
      }
      this.inherited(arguments);
      if (this.conf.templates && this.conf.templates.listbody) {
        this.listbody = domConstruct.create('div', null, this.domNode);
        const bodyNode = handlebars.run(this.listbody, this.conf, this.conf.templates.listbody, null, true);
        domConstruct.place(this.getView().domNode, bodyNode);
      }
    },
    updateRowClass() {
      if (this.conf.templates != null && this.conf.templates.rowexpand) {
        this.rowClass = declare(TemplateExpandRow, { initExpandTitles });
      } else if (this.conf.template != null) {
        this.rowClass = declare(MetadataExpandRow, { initExpandTitles });
      }

      return this.rowClass !== ListRow;
    },
    getRowClickLink(row) {
      if (this.conf.click) {
        const entry = row.entry;
        const prefix = config.hashParamsPrefix || 'esc_';
        return `${this.conf.click}#${prefix}entry=${entry.getId()}&${prefix}context=${entry.getContext().getId()}`;
      }
    },
    localeChange() {
      this.updateLocaleStrings(this.NLSBundle0, this.NLSBundle1);
    },

    showStopSign() {
      return false;
    },

    search(paramsParams) {
      if (this.conf.relation &&
                this.entry.getMetadata().find(this.entry.getResourceURI(), this.conf.relation).length === 0) {
        this.listView.showEntryList(new ArrayList({ arr: [] }));
      } else {
        const qo = this.getSearchObject(paramsParams ? paramsParams.term : undefined);
        this.listView.showEntryList(qo.list());
      }
    },

    getSearchObject(term) {
      const es = defaults.get('entrystore');
      const so = es.newSolrQuery();
      if (this.conf.relation) {
        const stmts = this.entry.getMetadata().find(this.entry.getResourceURI(), this.conf.relation);
        const rels = stmts.map(stmt => stmt.getValue());
        so.resource(rels);
      }

      if (this.conf.limit) {
        so.limit(this.conf.limit);
      }

      if (this.contextId) {
        so.context(this.contextId);
      } else if (this.conf.rowcontext === 'inherit' && this.conf.context) {
        so.context(this.conf.context);
      } else if (this.conf.rowcontext) {
        if (this.conf.rowcontext !== '') {
          so.context(this.conf.rowcontext);
        }
      }
      if (this.conf.relationinverse && this.entry) {
        so.uriProperty(this.conf.relationinverse, this.entry.getResourceURI());
      }

      if (this.conf.constraints) {
        constraints(so, this.conf.constraints);
      }

      if (this.block === 'searchList' && this.conf.headless) {
        filter.constraints(so);
      }
      if (this.conf.facets) {
        filter.facets(so);
      }

      if (this.conf.rdftype) {
        so.rdfType(this.conf.rdftype);
      }

      if (term != null && term.length > 0) {
        (Array.isArray(term) ? term : [term]).forEach((t) => {
          so.or({
            title: t,
            description: t,
            'tag.literal': t,
          });
        });
      }

      so.publicRead();

      return so;
    },
  });
});
