define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-style",
    "config",
    "rdforms/view/Presenter",
    "entryscape-commons/list/EntryRow",
    "./MetadataExpandRow",
    "entryscape-commons/defaults",
    "./List",
    "./formats"
], function (declare, array, domConstruct, domAttr, domStyle, config, Presenter, EntryRow, MetadataExpandRow, defaults, List, formats) {

    // TODO Check if this renderer is ever used (search is used instead)
    return function(node, data, items) {
        var es = defaults.get("entrystore");
        var cid = data.context || config.urlParams.context || config.econfig.context;
        var eid = data.entry || config.urlParams.entry || config.econfig.entry;

        if (cid != null && eid != null) {
          es.getEntry(es.getEntryURI(cid, eid)).then(function (entry) {
            if (entry.getMetadata().find(entry.getResourceURI(), data.relation).length > 0) {
              data.headless = true;
              var sl = new List({
                conf: data,
                itemstore: items,
                entry: entry
              }, domConstruct.create("div", null, node));
              sl.show();
            } else {
              //TODO write "Nothing to show" somehow?
            }
          });
        } else {
          var sl = new List({
            conf: data,
            itemstore: items,
            contextId: cid,
          }, domConstruct.create("div", null, node));
          sl.show();
        }
    };
});