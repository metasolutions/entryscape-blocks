define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "rdforms/view/Presenter",
    "entryscape-blocks/boot/handlebars",
    "entryscape-commons/list/common/ExpandRow"
], function (declare, domConstruct, Presenter, handlebars, ExpandRow) {

    return declare([ExpandRow], {
        showCol3: false,

        render() {
            var conf = this.list.conf;
            if (!conf.templates || !conf.templates.rowhead) {
                return this.inherited(arguments);
            }
            handlebars.run(this.nameNode, {
                htemplate: conf.templates.rowhead,
                context: this.entry.getContext().getId(),
                entry: this.entry.getId()
            }, null, this.entry);
        },

        getRenderNameHTML: function() {
            var name = this.getRenderName();
            var href = this.list.getRowClickLink(this);
            return href ? "<a href=\""+href+"\">"+name+"</a>" : name;
        },

        initExpandArea: function(node) {
            var p = new Presenter({compact: this.list.conf.onecol !== true}, domConstruct.create("div", {style: {"padding": "0px 0px 10px 15px"}}, node));
            var template = this.list.itemstore.getItem(this.list.conf.template);
            p.show({resource: this.entry.getResourceURI(),
                graph: this.entry.getMetadata(), template: template});
        }
    });
});