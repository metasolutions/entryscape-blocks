# EntryScape Blocks 
Allows you to embed information from EntryScape in various websites or CMS:es

## Principles
EntryScape Blocks consists of a set of UI-components that can be inserted into a web page to enrich it with information fetched from the EntryScape backend (EntryStore).

To include a block in a webpage two things needs to be done:

1. An element needs to be marked to trigger a certain block which often includes providing some parameters.
2. The blocks javascript must be included in the webpage to detect all marked elements and render the corresponding blocks.

## Example markup with attributes

Lets look at a small but complete example where we render the title of entry 1 from context 1 from the entrystore instance at http://example.com/store.

### Option 1: Single attribute as JSON (Recommended approach)
This is the preferred option for describing a block if the amount of parameters and their values are reasonable small.

    <span data-entryscape='{"block": "text", "entry": "1", "context": "1", "entrystore": "http://example.com/store"}'></span>

### Option 2: Multiple attributes (Kept for backwards compatability)
This option is seldom used, although it was the first approach taken and is kept for backwards compatability. Also, it can sometimes be easier to read as one level of quotes are avoided.

    <span data-entryscape="true" 
          data-entryscape-block="text"
          data-entryscape-entry="1"
          data-entryscape-context="1"
          data-entryscape-entrystore="http://example.com/store"></span>

### Option 3: Attributes in JSON as special script tag (used for many parameters and large values)
    <script type="text/x-entryscape-json" data-entryscape="true">
      {
         "block": "text",
         "entry": "1",
         "context": "1",
         "entrystore": "http://example.com/store"
      }
    </script>

## Default parameters
The current implementation of blocks can only communicate with one entrystore instance per page. Hence, if a page has several blocks it is enough to provide the 'entrystore' parameter in one of the blocks.
Another option is to global variable with this information:

    <script>
      __entryscape_config = {
        entrystore: {
          repository: "http://localhost:8080/store/"
        }
      };
    </script>

Note that the global `__entryscape_config` variable is the same as provided in the config/local.js which is included in the build. Hence, it is possible to build a special version of EntryScape Blocks that will default to a specific entrystore instance.

## Page parameters
A common scenario is to have one webpage for searching and one webpage to see the details for each search result. In this case you cannot hardcode the entry as a parameter to the block, instead it has to be detected from page parameters. Insert page parameters like this:

    https://example.com/detailspage#esc_entry=1&esc_context=1

If you want to change to another expression you can specify this in the global `__entryscape_config` variable like this (to avoid potential conflicts):

     __entryscape_config = {
        //  This is the default value
        hashParamsPrefix: "esc_"
     };

## Relation and inverseRelation
Sometimes blocks needs to retrieve related entries to the entry provided by the page parameter. For example, a page may want to display information about both an organization and the related site (via org:hasSite property) it is located at. Many of the blocks support the relation and inverseRelation parameters to indicate the property to follow from the current entry, e.g.:

    Organization: <span data-entryscape='{"block": "text"}'></span><br>
    At site: <span data-entryscape='{"block": "text", relation: "org:hasSite"}'></span>
 
## Define and use
Fetching a related entry nearly always requires a request to entrystore and will cause a small delay. If multiple blocks wants to present information about a single related entry it is clearly unneccessary to request the same entry multiple times. The define and use mechanism lets you avoid this problem by allowing one block be the main responsible for requesting an entry and then "define" it when it is loaded.
The blocks that have stated they want to "use" an entry will get rendered first when it is available. Lets continue with the example above but now you want to have the name of the site in a h3 element and the rest of the metadata below.

    Site <h3><span data-entryscape='{"block": "text", "relation": "org:hasSite", "define": "site"}'></span></h3>
    <p data-entryscape='{"block": "viewMetadata", "template": "org:Site" "use": "site"}'></span>

## Handlebar templates
A few of the blocks requires handlebars templates as parameters that can be quite large. To avoid awkward escaping of a lot of characters there is a special type of script tag that can be used. See an example of how this is done for the template block below.

    <script type="text/x-entryscape-handlebar" data-entryscape='{"block": "template"}'>
      <h4>{{text}}</h4>
      <div class="description">{{text content="${dcterms:description}"}}</div>
    </script>

Note that all EntryScape blocks are made available as helpers within the handlebar template. That means that it is possible to have blocks inside of blocks. In the example above the "template" block uses the "text" block inside of the handlebar template. The parameters to the block are simply provided as a space separated list of `param="value"`.

For the "list" block up to 4 different handlebar templates may be provided, this is achieved by separating them with handlebars special 'raw' blocks.

    <script type="text/x-entryscape-handlebar" data-entryscape='{"block": "search"}'>
      {{{{listplaceholder}}}}
        <h4>No matching results</h4>
      {{{{/listplaceholder}}}}
      {{{{rowhead}}}}
        <h4>{{text}}</h4>
        <div class="description">{{text content="${dcterms:description}"}}</div>
      {{{{/rowhead}}}}
    </script>
TODO templates withtin templates

## Avaliable blocks

### text
Can be used without any parameter, if so it tries to find a suitable title for the current entry. This behaviour can be overridden by providing the `content` parameter where anything looking like `${ns:prop}` will be replaced with the corresponding value of the property (if it exists). Compared with the `prop` helper inside of the template block this allows a more compact syntax, e.g. to express a firstname and lastname:

    <span data-entryscape='{"block": "text", "content"="${vc:firstName} ${vc:lastName}"}'></span>

For producing simple texts this is more compact than the template block approach:

    <script type="text/x-entryscape-handlebar" data-entryscape='{"block": "template"}'>
      {{prop vc:firstName}} {{prop vc:lastName}}
    </script>

However, the content parameter of the text block does not allow conditionals, loops or nested blocks.

### link
The link block produces the same text as the text block but makes it into a link by using the `click` parameter. E.g.

    <span data-entryscape='{"block": "link", "click"="/otherpage"}'></span>

The link to the "/otherpage" will have the page parameters appended (see section on page parameters above) for the entry relevant for the link block. If you want to focus on another entry than the one you have for the current page you have four options:
 1. Explicitly set the entry via entry and context parameters.
 2. Provide a `relation` or `relationInverse` parameter to take one step away from the current entry.
 3. Make use of the `use` parameter (indirectly the same as using relation or relationInverse).
 4. Have the link inside of a list row will use the entry displayed in that row.

### template
This block allows you to write handlebar templates which may contain additional blocks as helpers. In addition to blocks there is also a few specific helpers:

#### ifprop
Conditional is true if a certain property exists, e.g.:

    {{ifprop foaf:firstName}}
      {{prop foaf:firstName}} {{prop foaf:lastName}}
    {{/ifprop}}

#### eachProp
Loop that goes through values for a certain property, e.g.:

    {{#eachprop "dcat:theme"}}<span class="tema">{{label}}</span>{{/eachprop}}

Within the loop a few special values are available:
* value - the value of the property
* type - the type of the value (uri, literal or blank)
* lang - the language code for the value (if any)
* datatype - the datatype of the value (if any)
* label - a nice label for the value (first tries to find one from loaded RDForms templates and then from loaded entries)
* description - a description from a matching choice in a loaded RDForms template (if any)

#### prop
Renders the value given a property, in addition the parameter `render` may be given with any of the values specified for `eachProp`. Here is an example:

    {{prop "dcat:theme" render="label"}}

### image
Renders an image by finding a URL from a specified property given in the parameter `property`. It also sets the size from the parameters `width` and `height`, e.g.:

    <span data-entryscape='{"block": "image", "property": "foaf:depiction", "width": "300px"}'></span>
### viewMetadata
Renders metadata of an entry with help of an RDForms template. E.g.:

    <span data-entryscape='{"block": "viewMetadata", "template": "dcat:OnlyDataset", "filterpredicates":"dcterms:title,dcterms:description", "onecol": "true"}'></span>

To avoid constructing a lot of templates it is possible to exclude certain fields from the template via the parameter `filterpredicates`. This is very useful when you want to have special treatment of certain fields, e.g. have the dcterms:title in the title and not in the complete metadata view.

Do not forget that you must load appropriate RDForms bundles with the templates you need.

With the onecol argument set to true fieldnames will be one row and the value beneth, the alternative (and default) is to have the first level fields on the same row as the fieldname to make the rendering more vertically compact.
### list
### formatList
### catalogList

### search
### facets
### searchInput
### multiSearch  
### searchFilter

### config
This block provides configuration options that have no direct visual rendering but provides configurations that are used by one or several other blocks.

#### namespaces
This is an object that provides a map from the abbreviation to the full namespace. This map will be registered and centrally and will help you write the rest of the configurations. For example:

     "namespaces": {
             "ex": "http://example.com/"
     }

Observe that a bunch of common namespaces are already registered like: *rdf*, *rdfs*, *dc*, *dcterms*, *foaf*, *skos*, *dcat*, *xsd*, *ical*, *vcard*, *owl*, *wot*, *org*, *vs*, *gn*, *schema*.
You can look them up using [prefix.cc](http://prefix.cc).

Namespaces may also be declared in the global `__entryscape_config` variable within the itemstore object.

    <script>
      __entryscape_config = {
        itemstore: {
         "namespaces": {
           "schema": "http://schema.org/"
         }
        }
      };
    </script>

#### bundles
In addition to the RDForms template bundles provided by default in blocks (includes support for dcat, dcterms, foaf) you can indicate additional template bundles to be loaded like this:

       "bundles": [
         "https://example.com/my/template/bundle.js"
       ],
       
Bundles may also be declared in the global `__entryscape_config` variable within the itemstore object.

    <script>
      __entryscape_config = {
        itemstore: {
         "bundles": [
           "https://tora.entryscape.net/theme/tora.js",
           "https://tora.entryscape.net/libs/rdforms-templates/schema.org/schema.js"
         ]
        }
      };
    </script>

#### collections

Collections are used to populate the facet and multisearch blocks. There are five ways to provide collections as indicated by the `type` attribute:
1. **inline** - an array of objects with label and value in the `list` attribute.
2. **rdforms** - reuse "choices" from RDForms template choice items as they already provides label and values. Reuse by providing item id in the `templatesource` attribute.
3. **preload** - preload collection from entrystore, this is done by providing the constraining attributes `rdftype` and an optional `context`.
4. **search** - an solr search will be performed to yield a collection of the matching entries, typically by combining a given `rdftype` attribute with a generic search term, for instance from typeahead input field.
5. **facet** - the latest solr search performed by the search block (with the facet flag) will yield a collection from the facets in the results. The `property` attribute is mandatory to indicate the facet and an `nodetype` must be specified to be `literal` if the facet values are not to be treated as URIs which is the default.

#### entitytypes and type2template

When using rdforms to show metadata it is sometimes neccessary to provide an indication which form to use for related entities in the form. Therefore you can provide a map between type of the entity and which RDForms template to use, like this:

    "type2template": {
      "foaf:Agent": "idOfTemplateForAgent"
    }

In some situations you need more complicated ways of matching entities to templates, e.g. when a simple type is not unique enough. In the following example both countries and continents are represented as gn:FeatureCode, but we can make the distinction based on the gn:featureCode property instead. For the country we need to capture a range of different featureCodes since they all correspond to countries in different legal states, this is done by providing an array for the property, it is interpreted as a disjunction when searching. Since the simple map form cannot be used we use the entitytypes attribute instead like this:

    "entitytypes" : {
      "country": {
          constraints: {
            "rdf:type": "gn:Feature",
            "gn:featureCode": [
              "gn:A.PCLD", "gn:A.PCLI","gn:A.PCLIX", "gn:A.PCLH", 
              "gn:A.TERR", "gn:A.PCLS", "gn:A.PCLF", "gn:A.PCL"]
            },
          },
          template: "countryTemplateId",
      },
      "continent": {
          constraints: {
            "rdf:type": "gn:Feature",
            "gn:featureCode": ["gn:L.CONT"]
          },
          template: "continentTemplateId",
      },
    }   

## Wordpress plugin
With the help of the wordpress plugin it is possible to provide blocks using the shortcode syntax which will be translated to the original expressions described above. For example, the following shortcode:

    [entryscape block="text" content="${vc:firstName} ${vc:lastName}"]

Translates to the example given in documentation of the text block above, repeated here for clarity and ease of comparision:

    <span data-entryscape='{"block": "text", "content"="${vc:firstName} ${vc:lastName}"}'></span>

For the blocks that requires expressing handlebar templates we also need a closing shortcode:

    [entryscape block="template"]
      <h4>{{text}}</h4>
      <div class="description">{{text content="${dcterms:description}"}}</div>
    [/entryscape]

Which corresponds to the example taken in the section introducing handlebar templates, again repeated here for clarity and ease of comparision:

    <script type="text/x-entryscape-handlebar" data-entryscape='{"block": "template"}'>
      <h4>{{text}}</h4>
      <div class="description">{{text content="${dcterms:description}"}}</div>
    </script>

For more information about the wordpress plugin, see the documentation in the plugins/wordpress folder.