define([
  'dojo/_base/array',
  'dojo/_base/lang',
  'dojo/string',
  'rdforms/utils',
  'entryscape-commons/defaults',
], (array, lang, string, utils, defaults) => {
  const rdfutils = defaults.get('rdfutils');
  const special = {
    label(entry) {
      return rdfutils.getLabel(entry);
    },
  };
  const varRegexp = /\$\{([^\s\:\}]*)(?:\:([^\s\:\}]+))?\}/g;

  return function (data, entry) {
    if (data.content) {
      let content = data.content;
      const vars = array.map(content.match(varRegexp), v => v.substr(2, v.length - 3));
      const defaultProj = {};
      const vals = {};
      const mapping = {};
      array.forEach(vars, (v) => {
        let nv = v;
        let fallback = '';
        const arr = v.split('|');
        if (arr.length > 1) {
          nv = arr[0];
          fallback = arr[1];
        }
        if (special[nv]) {
          vals[nv] = special[nv](entry) || fallback;
        } else {
          const vp = nv.replace(':', '_');
          defaultProj[vp] = fallback;
          mapping[vp] = nv;
          content = content.replace(new RegExp(`\\\${${v.replace('|', '\\|')}}`, 'g'), `\${${vp}}`);
        }
      });
      const pr = entry.getMetadata().projection(entry.getResourceURI(), mapping, 'statement');
      Object.keys(mapping).forEach((key) => {
        const stmts = pr[`*${key}`];
        const lmap = {};
        stmts.forEach((stmt) => {
          if (stmt.getLanguage()) {
            lmap[stmt.getLanguage()] = stmt.getValue();
          }
        });
        if (Object.keys(lmap).length > 0) {
          pr[key] = utils.getLocalizedValue(lmap).value;
        }
      });
      const obj = lang.mixin(vals, defaultProj, pr);
      return string.substitute(content, obj);
    }
    return rdfutils.getLabel(entry);
  };
});
