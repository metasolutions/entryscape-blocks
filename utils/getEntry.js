define([
    "entryscape-commons/defaults",
    "entryscape-blocks/boot/params",
    "store/Entry",
    "config",
], function(defaults, params, Entry, config) {
    var es = defaults.get("entrystore");
    var esu = defaults.get("entrystoreutil");

    return function(data, callback) {
        var cid, eid;

        var f = function(entry) {
            if (data.define) {
                defaults.set("blocks_"+data.define, entry);
            }
            return entry;
        };

        var useRelation = function(entry) {
            if (data.relationinverse) {
                var qo = es.newSolrQuery().uriProperty(data.relationinverse, entry.getResourceURI());
                if (data.rdftype) {
                    qo.rdfType(data.rdftype);
                }
                qo.limit(1).list().getEntries().then(function(arr) {
                    if (arr.length > 0) {
                        f(arr[0]);
                        callback(arr[0]);
                    }
                });
            } else if (data.relation) {
                var relatedResource = entry.getMetadata().findFirstValue(entry.getResourceURI(), data.relation);
                if (relatedResource) {
                    esu.getEntryByResourceURI(relatedResource).then(f).then(callback);
                }
            }
        };

        if (data.use) {
            defaults.onChange("blocks_"+data.use, function(entry) {
                if (data.relation || data.relationinverse) {
                    useRelation(entry);
                } else {
                    callback(entry);
                }
            }, true);
        }

        var onEntry = function(entry) {
            if (data.relation || data.relationinverse) {
                useRelation(entry);
            } else if (data.rdftype) {
                esu.getEntryByType(data.rdftype, cid ? es.getContextById(cid) : null).then(f).then(callback);
            } else {
                f(entry);
                callback(entry);
            }
        };
        if (data.entry instanceof Entry) {
            onEntry(data.entry);
        }
        params.onInit(function(urlParams) {
            var ncid = data.context || urlParams.context || config.econfig.context;
            var neid = data.entry || urlParams.entry || config.econfig.entry;
            if (ncid === cid && neid === eid) {
                callback();
                return;
            }
            cid = ncid;
            eid = neid;
            es.getEntry(es.getEntryURI(cid, eid)).then(onEntry);
        });
    };
});