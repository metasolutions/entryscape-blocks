define([
  'entryscape-commons/defaults',
  'rdfjson/namespaces',
  './labels',
], (defaults, namespaces, labels) => {
  const shorten = (value) => {
    if (value.length > 15) {
      const hidx = value.lastIndexOf('#');
      const sidx = value.lastIndexOf('/');
      return hidx > sidx ? value.substr(hidx + 1) : value.substr(sidx + 1);
    }
    return value;
  };

  const add = function (filter, option) {
    const group = option.group || 'term';
    let arr = filter[group];
    if (!arr) {
      arr = [];
      filter[group] = arr;
    }
    if (!arr.find(item => item.value === option.value)) {
      arr.push(option);
    }
  };

  let lock = false;
    // Timeout for unlock since hash change is detected via polling (dojo/hash)
    // and has a delay of 100 ms
  const unlock = function () {
    setTimeout(() => {
      lock = false;
    }, 150);
  };
  const filterObj = {
    clear() {
      if (lock) {
        return;
      }
      lock = true;
      defaults.set('blocks_search_filter', {});
      unlock();
    },
    setAll(options) {
      if (lock) {
        return;
      }
      lock = true;
      const filter = {};
      (options || []).forEach((option) => {
        add(filter, option);
      });
      defaults.set('blocks_search_filter', filter);
      unlock();
    },
    addAll(options) {
      if (lock) {
        return;
      }
      lock = true;
      const filter = defaults.get('blocks_search_filter') || {};
      (options || []).forEach((option) => {
        add(filter, option);
      });
      defaults.set('blocks_search_filter', filter);
      unlock();
    },
    add(option) {
      if (lock) {
        return;
      }
      lock = true;
      const filter = defaults.get('blocks_search_filter') || {};
      add(filter, option);
      defaults.set('blocks_search_filter', filter);
      unlock();
    },
    remove(option) {
      filterObj.replace(option);
    },
    replace(oldOption, newOption) {
      if (lock) {
        return;
      }
      lock = true;
      const filter = defaults.get('blocks_search_filter') || {};
      if (oldOption) {
        const group = oldOption.group || 'term';
        const arr = filter[group];
        if (arr) {
          arr.splice(arr.indexOf(oldOption), 1);
          if (arr.length === 0) {
            delete filter[group];
          }
        }
      }
      if (newOption) {
        add(filter, newOption);
      }
      defaults.set('blocks_search_filter', filter);
      unlock();
    },
    constraints(obj) {
      const filter = defaults.get('blocks_search_filter') || {};
      const filterIdx = {};
      (defaults.get('blocks_collections') || []).forEach((c) => {
        filterIdx[c.name] = c;
      });
      Object.keys(filter).forEach((key) => {
        const vals = filter[key].map((v) => {
          if (typeof v === 'string') {
            return v;
          }
          return v.value;
        });
        switch (key) {
          case 'tags':
            return;
          case 'term':
            vals.forEach((v) => {
              obj.or({
                title: v,
                description: v,
                'tag.literal': v,
              });
            });
            return;
          case 'type':
            obj.rdfType(vals);
            return;
          default:
        }
        const filterDef = filterIdx[key];
        const prop = filterDef.property;
        if (namespaces.expand(prop) === namespaces.expand('rdf:type')) {
          obj.rdfType(vals);
        } else if (filterDef.nodetype === 'literal') {
          obj.literalProperty(prop, vals);
        } else {
          obj.uriProperty(prop, vals);
        }
      });
      return obj;
    },
    facets(obj) {
      const collections = defaults.get('blocks_collections');
      collections.forEach((def) => {
        if (def.includeAsFacet) {
          switch (def.nodetype) {
            case 'integer':
              obj.integerFacet(def.property);
              break;
            case 'literal':
              obj.literalFacet(def.property);
              break;
            default:
              obj.uriFacet(def.property);
          }
        }
      });
    },
    facets2collections(facetIdx) {
      const collections = defaults.get('blocks_collections');
      collections.forEach((def) => {
        const facet = facetIdx[namespaces.expand(def.property)];
        const group = def.name;
        if (facet) {
          def.changeLoadLimit = (limit) => {
            def.loadedLimit = limit;
            if (def.nodetype === 'literal') {
              def.source = facet.values.map(value => ({
                label: value.name,
                value: value.name,
                group,
                occurence: value.count,
              }));
              def.list = (limit && facet.values.length > limit) ?
                        def.source.slice(0, limit) : def.source;
              defaults.set(`blocks_collection_${def.name}`, def);
            } else {
              let values = facet.values;
              if (limit && values.length > limit) {
                values = values.slice(0, limit);
              }
              const svalues = values.map(value => value.name);
              labels(svalues, def.nodetype).then((lbls) => {
                def.source = values.map(value => ({
                  label: lbls[value.name] || shorten(value.name),
                  value: value.name,
                  group,
                  occurence: value.count,
                }));
                def.list = def.source;
                defaults.set(`blocks_collection_${def.name}`, def);
              });
            }
          };
          def.changeLoadLimit(def.limit);
        }
      });
    },
  };
  return filterObj;
});
