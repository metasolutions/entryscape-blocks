define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-attr',
  'dojo/dom-style',
  'entryscape-blocks/boot/params',
  'entryscape-commons/defaults',
  'entryscape-blocks/utils/filter',
  'store/Entry',
  'jquery',
  'selectize',
], (declare, domConstruct, domClass, domAttr, domStyle, params, defaults, filter, Entry,
             jquery) => {
  const rdfutils = defaults.get('rdfutils');

    /**
     * Renders a dropdown filter with typeahead functionality.
     * Selected values will be used by the "search" component (renderSearchList) as constraints.
     *
     * Depending on the parameters provided it works a bit differently:
     *   rdftype - requests will be made to entrystore to retrieve all entries with this type
     *   context - restrict a search to the specified context,
     *             if set to "true" it uses the context from the urlParams.
     *   collection - name of a collection from where values will be taken,
     *              the collection is typically provided via a preloaded component
     *   property - the property to filter the selected values with.
     *   literal - true if the values are to be considered literals.
     */
  return function (node, data, items) {
    domClass.add(node, 'block_searchFilter');
    if (typeof data.width !== 'undefined') {
      domStyle.set(node, 'width', data.width);
    }
    let urlParams = {};
    params.onInit((up) => {
      urlParams = up;
    });

    const input = domConstruct.create('input', { type: 'text', placeholder: 'anything' }, node);
    let selectize;
    let selectedOption;
    let lock = false;

    const settings = {
      valueField: 'value',
      labelField: 'label',
      searchField: 'label',
      allowEmptyOption: data.allowEmptyOption !== false,
      mode: 'single',
      closeAfterSelect: true,
      preload: 'focus',
      create: false,
      onItemAdd(value) {
        const newOption = value !== '' ? selectize.options[value] : undefined;
        lock = true;
        filter.replace(selectedOption, newOption);
        lock = false;
        selectedOption = newOption;
      },
      render: {
        option(d, escape) {
          const label = d.value === '' ? data.emptyLabel || '&nbsp;' : escape(d.label);
          const occurence = typeof d.occurence !== 'undefined' ? ` (${d.occurence})` : '';
          return `<div>${label}${occurence}</div>`;
        },
        item(d, escape) {
          return `<div>${d.value === '' ? data.emptyLabel || '&nbsp;' : escape(d.label)}</div>`;
        },
      },
    };

    const collectionName = `blocks_collection_${data.collection}`;
    settings.load = function (query, callback) {
      const collection = defaults.get(collectionName);
      if (collection.type === 'search') {
        const es = defaults.get('entrystore');
        const qo = es.newSolrQuery().publicRead();
        const context = (data.context === true ? urlParams.context : data.context)
          || collection.context;
        if (context) {
          qo.context(context);
        }
        if (collection.rdftype) {
          qo.rdfType(collection.rdftype);
        }
        const term = query.length ? query : '*';
        if (collection.searchproperty) {
          qo.literalProperty(collection.searchproperty, [term, `${term}*`]);
        } else {
          qo.title(term);
        }

        qo.limit(10).list().getEntries().then((arr) => {
          callback(arr.map(entry => ({
            value: entry.getResourceURI(),
            label: rdfutils.getLabel(entry),
            group: data.collection,
          })));
        }, () => {
          callback();
        });
      } else if (collection.list && collection.list.length > 0) {
        if (collection.list[0] instanceof Entry) {
          callback(collection.list.map(entry => ({
            value: entry.getResourceURI(),
            label: rdfutils.getLabel(entry),
            group: data.name,
          })));
        } else {
          callback(collection.list);
        }
      } else if (collection.type === 'facet') {
        defaults.get("blocks_collection_"+collection.name, (col) => {
          callback(collection.list);
        });
      }
    };
    // Initialize after load function is added
    selectize = jquery(input).selectize(settings)[0].selectize;

    defaults.onChange('blocks_search_filter', (filters) => {
      if (lock) {
        // If the filter is itself making the change
        return;
      }
      lock = true;
      const selIt = filters[data.collection];
      if (selIt) {
        if (!selectedOption || selectedOption.value !== selIt[0].value) {
          selectize.addItem(selIt[0].value);
        }
      } else if (selectedOption) {
        selectize.removeItem(selectedOption.value);
      }
      lock = false;
    });
  };
});
