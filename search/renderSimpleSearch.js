define([
  'dojo/_base/declare',
  'dojo/dom-construct',
  'dojo/dom-class',
  'dojo/dom-attr',
  'dojo/dom-style',
  'dojo/on',
  'entryscape-blocks/utils/filter',
], (declare, domConstruct, domClass, domAttr, domStyle, on, filter) =>
  (node, data) => {
    /**
     * Renders a search input field. The typed search term will be used as a constraint by the
     * search component.
     */

    if (typeof data.width !== 'undefined') {
      domStyle.set(node, 'width', data.width);
    }
    domClass.add(node, 'block_searchInput');
    let input;
    let t;
    let term;
    const searchTriggered = () => {
      let newTerm = domAttr.get(input, 'value');
      newTerm = newTerm === undefined || newTerm.length <= 2 ? undefined :
        { value: newTerm, group: data.collection || 'term' };
      filter.replace(term, newTerm);
      term = newTerm;
    };
    if (data.plainInput) {
      input = domConstruct.create('input', {
        type: 'text',
        class: 'form-control',
        placeholder: data.placeholder,
        title: data.placeholder,
      }, node);
    } else {
      const inputgroup = domConstruct.create('span', { class: 'input-group' }, node);
      input = domConstruct.create('input', {
        type: 'text',
        class: 'form-control',
        placeholder: data.placeholder,
        title: data.placeholder,
      }, inputgroup);
      const igb = domConstruct.create('span', { class: 'input-group-btn' }, inputgroup);
      const button = domConstruct.create('button', { class: 'btn btn-default' }, igb);
      domConstruct.create('span', { 'aria-hidden': true, class: 'fa fa-search' }, button);
      on(button, 'click', searchTriggered);
    }
    on(input, 'keyup', () => {
      if (t != null) {
        clearTimeout(t);
      }
      t = setTimeout(searchTriggered, 300);
    });
  });
