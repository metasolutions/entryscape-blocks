define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "entryscape-blocks/boot/params",
    "entryscape-commons/defaults",
    "entryscape-blocks/list/List",
    "entryscape-blocks/utils/getEntry",
    "entryscape-blocks/utils/constraints",
    "config",
    "jquery",
], function (declare, domConstruct, params, defaults, List, getEntry, constraints, config, jquery) {

    return function(node, data, items) {

        const sl = new List({
            block: "searchList",
            conf: data,
            itemstore: items,
            includeHead: !data.headless
        }, domConstruct.create("div", null, node));
        if (!sl.includeHead) {
            jquery(sl.domNode).find(".panel").removeClass("panel");
            jquery(sl.domNode).addClass("headLess");
        }
        if (data.headless) {
            defaults.onChange("blocks_search_filter", function(filter) {
                if (filter.term) {
                    sl.search({}); //Provided via the filter filter.constraint method.
                } else {
                    sl.search({term: "*"});
                }
            }, true);
        }

        params.onInit(function(urlParams) {
          sl.contextId = data.context || urlParams.context || config.econfig.context;
          if (sl.contextId != null && (data.entry || urlParams.entry || config.econfig.entry) != null) {
            getEntry(data, function (entry) {
              sl.entry = entry;
              if (!data.facets || data.initsearch) {
                sl.show();
              }
            });
          } else if (!data.facets || data.initsearch) {
            sl.show();
          }
        });
    };
});