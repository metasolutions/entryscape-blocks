define([
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-attr",
    "dojo/dom-style",
    "dojo/Deferred",
    "dojo/promise/all",
    "dojo/on",
    "rdfjson/namespaces",
    "entryscape-blocks/utils/filter",
    "entryscape-blocks/utils/labels",
    "entryscape-blocks/boot/params",
    "entryscape-commons/defaults",
    "store/Entry",
    "jquery",
    "selectize"
], function (declare, domConstruct, domClass, domAttr, domStyle, Deferred, all, on,
             namespaces, filter, labels, params, defaults, Entry, jquery) {
    var rdfutils = defaults.get("rdfutils");
    const termFilter = (arr, term) => {
        if (term.length > 0) {
            return arr.filter((val) => {
                return (val.label || "").toLowerCase().indexOf(term.toLowerCase()) > -1;
            });
        }
        return arr;
    };

    /**
     * Renders a dropdown filter with typeahead functionality.
     * Selected values will be used by the "search" component (renderSearchList) as constraints.
     *
     * Depending on the parameters provided it works a bit differently:
     *   rdftype - requests will be made to entrystore to retrieve all entries with this type
     *   context - restrict a search to the specified context,
     *             if set to "true" it uses the context from the urlParams.
     *   collection - name of a collection from where values will be taken,
     *              the collection is typically provided via a preloaded component
     *   property - the property to filter the selected values with.
     *   literal - true if the values are to be considered literals.
     */
    return function(node, data, items) {
        let lock = false;
        if (typeof data.width != "undefined") {
            domStyle.set(node, "width", data.width);
        }
        var urlParams = {};
        params.onInit(function(up) {
            urlParams = up;
        });

        defaults.get("blocks_collections", function(collections) {
            var name2col = {};
            collections.forEach(function(col) {
                name2col[col.name] = col;
            });

            var selectize;
            var localItems = {};
            var settings = {
                valueField: 'value',
                labelField: 'label',
                searchField: 'label',
                optgroupValueField: 'name',
                optgroupLabelField: 'label',
                optgroups: collections,
                optgroupField: 'group',
                mode: 'multi',
                closeAfterSelect: true,
                preload: 'focus',
                create: true,
                addPrecedence: true,
                persist: false,
                onItemAdd: function(value) {
                    if (lock) {
                      selectize.close();
                      return;
                    }
                    var item = selectize.options[value];
                    localItems[value] = item;
                    lock = true;
                    filter.add(item);
                    if (data.click) {
                        var urlParams = {};
                        let click = data.click;
                        const col = item.group && name2col[item.group] ? name2col[item.group] : {};
                        if (col.click) {
                            click = name2col[item.group].click;
                        }
                        const shortenedValue = namespaces.shortenKnown(item.value);
                        if (col.linkparam) {
                            switch (col.linkparam) {
                                case 'entry':
                                    if (item.eid && item.cid && col.linkparam) {
                                        urlParams.entry = item.eid;
                                        urlParams.context = item.cid;
                                    }
                                    urlParams.entry = item.eid;
                                    urlParams.context = item.cid;
                                    break;
                                case 'group':
                                    urlParams[item.group] = shortenedValue;
                                    break;
                                default:
                                    urlParams[col.linkparam] = shortenedValue;
                            }
                        } else {
                            urlParams[item.group || 'term'] = shortenedValue;
                            if (item.eid && item.cid) {
                                urlParams.entry = item.eid;
                                urlParams.context = item.cid;
                            }
                        }
                        params.setLocation(click || "", urlParams);
                    }
                    lock = false;
                  selectize.close();
                },
                onItemRemove: function(value) {
                    if (lock) {
                      selectize.close();
                      return;
                    }
                    var option = localItems[value];
                    delete localItems[value];
                    lock = true;
                    filter.remove(option || {value: value});
                    lock = false;
                  selectize.close();
                },
                render: {
                    option: function(data, escape) {
                        if (data.free) {
                            return '<div>Search for ' + escape(data.label) + '</div>';
                        }
                        return '<div>' + escape(data.label) + '</div>';
                    },
                    item: function(data, escape) {
                        var item = domConstruct.create('div', {'class': 'item'});
                        if (data.group && data.group !== 'term') {
                            domConstruct.create('span', {
                                'class': 'group',
                                'innerHTML': name2col[data.group].label + ":"
                            }, item);
                        }
                        domConstruct.create('span', {'class': 'itemLabel', 'innerHTML': escape(data.label)}, item);
                        on(domConstruct.create('i', {'class': 'fa fa-remove'}, item), 'click', () => {
                            selectize.removeItem(data.value);
                        });
                        return item;
                    },
                    option_create: function(data, escape) {
                        return '<div class="create">Search for "' + escape(data.input) + '"</div>';
                    }
                }
            };

            var input = domConstruct.create("input", {
                "type": "text",
                "placeholder": data.placeholder || "Search for..."
            }, node);
            var loads = collections.map((def) => query => {
              if (def.type === 'search') {
                    const es = defaults.get("entrystore");
                    const qo = es.newSolrQuery().publicRead();
                    const contextId = def.context || (data.context === true ? urlParams.context : data.context);
                    if (contextId) {
                        qo.context(contextId);
                    }
                    if (def.rdftype) {
                        qo.rdfType(def.rdftype);
                    }
                    const term = query.length > 0 ? query : "*";
                    if (def.searchproperty) {
                      qo.literalProperty(def.searchproperty, term);
                    } else {
                      qo.title(query);
                    }

                    return qo.limit(6).list().getEntries().then(arr => arr.map(entry => ({
                        value: entry.getResourceURI(),
                        eid: entry.getId(),
                        cid: entry.getContext().getId(),
                        label: rdfutils.getLabel(entry),
                        group: def.name
                    })));
                } else {
                  return new Promise(resolve =>
                      defaults.get("blocks_collection_"+def.name, () => {
                          resolve(termFilter(def.list, query));
                      }));
                }
            });

            settings.load = (query, callback) => {
                all(loads.map(function(ld) {
                    return ld(query);
                })).then(function(searchResults) {
                    var results = [];
                    var pos = 0;
                    var left = true;
                    while (results.length < 20 && left) {
                        left = false;
                        searchResults.forEach(function(arr) {
                            var v = arr[pos];
                            if (v) {
                                left = true;
                                if (v.label.toLowerCase().indexOf(query.toLowerCase()) !== -1) {
                                    results.push(v);
                                }
                            }
                        });
                        pos++;
                    }
                    callback(results);
                });
            };

            // Initialize after load function is added
            selectize = jquery(input).selectize(settings)[0].selectize;

            //Listen in and update search field if other parts of the ui changes the filter
            defaults.onChange("blocks_search_filter", function(filters) {
                if (lock) {
                    // If selectize is itself making the change
                    return;
                }
                lock = true;
                Object.keys(filters).forEach(function(group) {
                    var vals = filters[group];
                    if (!vals[0].label) {
                        defaults.get("blocks_collection_" + group, function(col) {
                            lock = true;
                            vals.forEach(function(item) {
                                var it = col.list.find(function(colItem) {
                                    return colItem.value === item.value;
                                });
                                var f = function(ite) {
                                    selectize.addOption(ite);
                                    localItems[ite.value] = ite;
                                    selectize.addItem(ite.value, true);
                                };
                                if (it) {
                                    f(it);
                                } else {
                                    labels([item.value]).then(function(uri2label) {
                                        f({label: uri2label[item.value], group, value: item.value});
                                    });
                                }
                            });
                            lock = false;
                        });
                    } else {
                        vals.forEach(function(item) {
                            selectize.addOption(item);
                            localItems[item.value] = item;
                            selectize.addItem(item.value, true);
                        });
                    }
                });
                selectize.items.forEach(function(value) {
                    var item = selectize.options[value];
                    var filt = filters[item.group || 'term'] || [];
                    if (!filt.find(function(fival) {
                            return fival.value === item.value;
                        })) {
                        delete localItems[item.value];
                        selectize.removeItem(item.value, true);
                    }
                });
                lock = false;
            }, true);
        });
    };
});