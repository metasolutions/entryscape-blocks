define([
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/on",
    "rdfjson/namespaces",
    "md5",
    "entryscape-blocks/boot/params",
    "entryscape-commons/defaults",
    "entryscape-blocks/utils/labels",
    "entryscape-blocks/utils/filter",
    "jquery",
], function (lang, declare, domConstruct, domStyle, domAttr, on, namespaces, md5, params, defaults,
             labels, filter, jquery) {
    var rdfutils = defaults.get("rdfutils");

    var FacetBlock = declare(null, {
        constructor: function(facetDef, node) {
            this.def = facetDef;
            this.domNode = domConstruct.create("div", {"class": "block_facet collection_"+facetDef.name}, node);
            this.headerNode = domConstruct.create("h3", {innerHTML: this.def.label}, this.domNode);
            this.bodyNode = domConstruct.create("ul", null, this.domNode);
            this.viewAllNode = domConstruct.create("button",
              { style: {display: "none" },
              class: "btn btn-default pull-right",
              innerHTML: "visa alla"}, this.domNode);
            const self = this;
            on(this.viewAllNode, "click", () => {
              if (self.def.loadedLimit > 0) {
                self.def.changeLoadLimit();
              } else {
                self.def.changeLoadLimit(self.def.limit);
              }
            })
            this.collectionName = "blocks_collection_"+facetDef.name;
            defaults.onChange(this.collectionName, lang.hitch(this, this.renderCollection), true);
        },

        renderCollection: function(collectionDef) {
            if (collectionDef.list) {
              this.render(collectionDef.list, defaults.get("blocks_search_filter") || {});
            }
        },
        renderFiltersUpdate: function(filters) {
            var collection = defaults.get(this.collectionName);
            if (collection) {
                this.render(collection, filters);
            }
        },
        render: function(collection, filters) {
            this.renderExpand(collection);
            const selectedItems = this.getSelectedItems(collection, filters);
            this.bodyNode.innerHTML = "";

            if (this.selectedMissingInCollection(selectedItems, filters)) {
                //Things missing in collection, only show selectedItems.
            } else {
                collection.forEach(function(item) {
                    this.drawOption(item, selectedItems.indexOf(item) !== -1)
                }, this);
            }
        },

        renderExpand: function(collection) {
          if (typeof this.def.limit === "undefined" || (this.def.limit > 0 &&
              collection.length < this.def.limit)) {
              // Nothing to expand
            domStyle.set(this.viewAllNode, "display", "none");
          } else if (this.def.loadedLimit > 0) {
            domAttr.set(this.viewAllNode, "innerHTML", "visa fler");
            domStyle.set(this.viewAllNode, "display", "inline-block");
          } else {
            domAttr.set(this.viewAllNode, "innerHTML", "visa färre");
            domStyle.set(this.viewAllNode, "display", "inline-block");
          }
        },

        selectedMissingInCollection: function(selectedItems, filters) {
            return false;
            /*let cfilter;
            if (filters && filters[this.def.name]) {
                cfilter = filters[this.def.name];
            }
            return cfilter && cfilter.length > selectedItems.length;*/
        },
        getSelectedItems: function(collection, filters) {
            let selectedItems = [];
            let cfilter;
            if (filters && filters[this.def.name]) {
                cfilter = filters[this.def.name];
                collection.find(function(item) {
                    cfilter.forEach(function(fvalue) {
                        if (item.value === fvalue.value) {
                            selectedItems.push(item);
                        }
                    });
                });
            }
            return selectedItems;
        },
        drawOption: function(item, selected) {
            const md = md5(item.value);
            var li = domConstruct.create("li", {'class': selected ? 'selected md5_'+md : 'md5_'+md}, this.bodyNode);
            domConstruct.create("span", {innerHTML: item.label, 'class': 'facetLabel'}, li);
            if (item.occurence) {
                domConstruct.create("span", {class: "occurence", innerHTML: "("+item.occurence+")"}, li);
            }
            if (selected) {
                var button = domConstruct.create("button", {class: "btn btn-small btn-link"}, li);
                domConstruct.create("i", {class: "fa fa-remove"}, button);
                on(button, "click", function(e) {
                    e.stopPropagation();
                    filter.remove(item);
                })
            }
            on(li, "click", function() {
                filter.add(item);
            });
        }
    });

    return function(node, data, items) {
        var fbs = [];
        defaults.onChange("blocks_collections", function(collections) {
            collections.forEach(function(collection) {
                if (collection.includeAsFacet) {
                    fbs.push(new FacetBlock(collection, node));
                }
            });
            var addListener = true;
            params.addListener(function(urlParams) {
                var constraints = [];
                collections.forEach((c) => {
                    if (c.includeAsFacet !== false) {
                        if (urlParams[c.name]) {
                            var arr = urlParams[c.name];
                            if (!Array.isArray(arr)) {
                                arr = [arr];
                            }
                            arr.forEach((f) => {
                                //var v = decodeURIComponent(f);
                                constraints.push({group: c.name, value: namespaces.expand(f)});
                            });
                        }
                    }
                });
                if (urlParams.term) {
                    if (Array.isArray(urlParams.term)) {
                      urlParams.term.forEach((t) => {
                        constraints.push({value: t, label: t, group: 'term'});
                      });
                    } else {
                      constraints.push({value: urlParams.term,
                        label: urlParams.term, group: 'term'});
                    }
                }
                filter.setAll(constraints);
                if (addListener) {
                    addListener = false;
                    defaults.onChange("blocks_search_filter", function(filter) {
                        delete urlParams.term;
                        collections.forEach((c) => {
                            if (c.includeAsFacet !== false) {
                                delete urlParams[c.name];
                            }
                        });
                        Object.keys(filter).forEach((key) => {
                            var vs = filter[key];
                            urlParams[key] = vs.map((v) => namespaces.shortenKnown(v.value));
                        });
                        params.setLocation("", urlParams);
                    });
                }
            });
        }, true);
    };
});